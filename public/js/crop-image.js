var $modal;
var image;
$(function () {
    let input = $('input[class*=file-input]');
    let cropper;

    input.on('change', (e) => {
        $modal = '';
        let files = e.target.files;
        $modal = $('#modal');
        image = $modal.find('.image-cropper-upload');
        let done = function (url) {
            input.val("");
            image.attr('src', url);
            $modal.modal('show');
        };

        let reader;
        let file;
        let url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = (e) => {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
        $modal.on('shown.bs.modal', () => {
            destroyCropper(cropper);
            cropper = new Cropper(image.get(0), {
                aspectRatio: 1 / 1,
                viewMode: 2,
                zoomOnWheel: true
            });
            initCropper(cropper)
        }).on('hide.bs.modal', () => {
            destroyCropper(cropper);
        });
    });

    let destroyCropper = (cropper) => {
        if (cropper)
            cropper.destroy();
    }

    let initCropper = (cropper) => {
        $('.crop').off('click').on('click', (e) => {
            const clickedItem = $(e.currentTarget);
            const $url = clickedItem.data('url');
            let crop = image.attr('data-crop');
            let canvasF, src, roundedCanvas;
            let canvas, $ext;
            if (cropper) {

                $('.ldBar').toggleClass("show");
                let croppedCanvas = cropper.getCroppedCanvas();
                roundedCanvas = getRectangleCanvas(croppedCanvas, "300", "300");
                $ext = roundedCanvas.toDataURL().includes('data:image/png') ? 'image/png' : 'image/jpeg';
                src = roundedCanvas.toDataURL();
                canvasF = roundedCanvas;
                $('.product-image').attr('src', roundedCanvas.toDataURL());
                canvasF.toBlob((blob) => {
                        let _token = $("meta[name='csrf-token']").attr("content");
                        let formData = new FormData();
                        formData.append('_token', _token);
                        formData.append('image', roundedCanvas.toDataURL());
                        $.ajax({
                            method: 'POST',
                            url: $url,
                            data: formData,
                            contentType: false,
                            processData: false,
                            xhr: function () {
                                let appXhr = $.ajaxSettings.xhr();
                                if (appXhr.upload) {
                                    appXhr.upload.addEventListener('progress', (e) => {
                                        updateProgress(e, bar1);
                                    }, false);
                                }
                                return appXhr;
                            },
                            success:  (data)=> {
                                $modal.modal('hide');
                                $('.ldBar').toggleClass("show");
                                $('.image-success').toggleClass("show");
                                $('input[name=image]').val(data.name)
                            },
                            error: function () {
                                $('.ldBar').toggleClass("show")
                            }
                        });

                    },
                    $ext, 2);
            }
        });
    }

    let getRectangleCanvas = (sourceCanvas, canvasWidth, canvasHeight) => {
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var width = canvasWidth;
        var height = canvasHeight;
        canvas.width = width;
        canvas.height = height;
        context.imageSmoothingEnabled = true;
        context.drawImage(sourceCanvas, 0, 0, width, height);
        context.globalCompositeOperation = 'destination-in';
        context.beginPath();
        context.rect(0, 0, canvasWidth, canvasHeight);
        // context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
        context.fill();
        return canvas;
    };

    let updateProgress = (e, progressbar) => {
        if (e.lengthComputable) {
            let currentProgress = Math.round((e.loaded / e.total) * 100); // Amount uploaded in percent
            progressbar.set(currentProgress);
        }
    };

});
