@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Products</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="products-table table nowrap">
                                <thead class="">
                                <tr>
                                    <th ><a href="{{route("products.add")}}"
                                                                       class="fa fa-plus fa-lg text-success"></a></th>
                                    <th >Product Name</th>
                                    <th >Product Type</th>
                                    <th >Author</th>
                                    <th >Publisher</th>
                                    <th >Selling Price</th>
                                    <th >Cost Price</th>
                                    <th >Edition</th>
                                    <th >Cover Type</th>
                                    <th >Product Size</th>
                                    <th >Product Weight</th>
                                    <th >#Nb Of Pages</th>
                                    <th >Release Date</th>
                                    <th >E-book Link</th>
                                    <th >Image</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <th scope="row"><span class="fas fa-bars"></span></th>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->type->name}}</td>
                                        <td>{{$product->author->full_name}}</td>
                                        <td>{{$product->publisher->name}}</td>
                                        <td>${{$product->selling_price}}</td>
                                        <td>${{$product->cost_price}}</td>
                                        <td>{{$product->productExtra->edition}}</td>
                                        <td>{{!is_null($product->productExtra->cover_type)?$coverTypes[$product->productExtra->cover_type]['name']:"-"}}</td>
                                        <td>{{!is_null($product->productExtra->size)?$productSizes[$product->productExtra->size]['name']." cm":"-"}}</td>
                                        <td>{{$product->productExtra->weight}}</td>
                                        <td>{{$product->productExtra->num_of_pages}}</td>
                                        <td>{{$product->productExtra->release_date}}</td>
                                        <td>{{$product->productExtra->pdf_link}}</td>
                                        <td>{!! $product->image?"<img class='img-circle' width='30px' src=".asset(\App\Models\Product::PRODUCTS_IMAGE_PATH.$product->image)."/>":"-" !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        $(function () {
            var table = $('.products-table').DataTable({
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                "ordering": false,
                paging: false,
                fixedColumns: true,
                // responsive: true,
                "autoWidth": true,

            });
            table.columns.adjust().draw();
        })
    </script>
@endsection