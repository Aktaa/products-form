@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form action="{{route("products.save")}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="card">
                        <div class="card-header">
                            <h4>Add Product</h4>
                        </div>
                        <div class="card-body">
                            <br>
                            <h3>Product Information</h3>
                            <hr>

                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Product Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="name">
                                        @if($errors->has("name"))
                                            <small class="form-text text-danger">{{$errors->first('name') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Category <span class="text-danger">*</span></label>
                                        <select name="category_id" class="select2 form-control category-select">
                                            <option value=""></option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("category_id"))
                                            <small class="form-text text-danger">{{$errors->first('category_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Category</label>
                                        <select name="sub_category_id" class="select2 form-control sub-category-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Series</label>
                                        <input type="number" name="series" class="form-control">
                                    </div>
                                    <br>

                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="">Product Image <span
                                                    class="far fa-check-circle text-success image-success fade"></span></label>
                                        <div class="image-container">
                                            <img class="product-image"
                                                 src="http://la-graine-biolande.lescigales.org/wp-content/uploads/2015/03/default-300x300.jpg"
                                                 alt="">
                                            <span class="image-product edit-image">
                                              <i class="fa fa-camera fa-2x center-vertical" title="Upload Image"
                                                 data-toggle="tooltip"></i>
                                        </span>
                                            <div class="ldBar" data-value="0" data-preset="energy"></div>
                                            <input type="file" accept="image/*"
                                                   name="file_input "
                                                   class="file-input sr-only">
                                            <input type="hidden" name="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Author <span class="text-danger">*</span></label>
                                        <select name="author_id" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($authors as $author)
                                                <option value="{{$author->id}}">{{$author->full_name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("author_id"))
                                            <small class="form-text text-danger">{{$errors->first('author_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Author Role<span class="text-danger">*</span> <span
                                                    title="ex: {{$roles_author_text}}"
                                                    data-toggle="tooltip"
                                                    class="fa fa-info-circle"></span></label>
                                        <select name="author_role_id" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($author_roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("author_role_id"))
                                            <small class="form-text text-danger">{{$errors->first('author_role_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Publisher <span class="text-danger">*</span></label>
                                        <select name="publisher_id" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($publishers as $publisher)
                                                <option value="{{$publisher->id}}">{{$publisher->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("publisher_id"))
                                            <small class="form-text text-danger">{{$errors->first('publisher_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Publisher Role<span class="text-danger">*</span> <span
                                                    title="ex: {{$roles_publisher_text}}" data-toggle="tooltip"
                                                    class="fa fa-info-circle"></span></label>
                                        <select name="publisher_role_id" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($publishers_roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("publisher_role_id"))
                                            <small class="form-text text-danger">{{$errors->first('publisher_role_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Financial Details</h3>
                            <hr>

                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Selling Price <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="number" class="form-control" name="selling_price">
                                        </div>
                                            @if($errors->has("selling_price"))
                                            <small class="form-text text-danger">{{$errors->first('selling_price') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Cost Price <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="number" class="form-control" name="cost_price">
                                        </div>
                                            @if($errors->has("cost_price"))
                                            <small class="form-text text-danger">{{$errors->first('cost_price') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Product Details</h3>
                            <hr>

                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>ISBN</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                                            </div>
                                            <input type="text" class="form-control" name="isbn">
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label>Product Type <span class="text-danger">*</span> <span
                                                    title="ex: {{$product_types_text}}" data-toggle="tooltip"
                                                    class="fa fa-info-circle"></span></label>
                                        <select name="type_id" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($product_types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has("type_id"))
                                            <small class="form-text text-danger">{{$errors->first('type_id') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Cover Type <span class="text-danger">*</span> <span
                                                    title="ex: {{$coverTypesText}}" data-toggle="tooltip"
                                                    class="fa fa-info-circle"></span></label>
                                        <select name="cover_type" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($coverTypes as $type)
                                                <option value="{{$type['id']}}">{{$type["name"]}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label>Product Weight</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                            class="fa fa-weight-hanging"></i></span>
                                            </div>
                                            <input type="text" class="form-control" name="weight">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Release Date</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="text" class="form-control date" name="release_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Barcode</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                                            </div>
                                            <input type="text" class="form-control" name="barcode">
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label>Edition</label>
                                        <input type="text" name="edition" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Product Size <span
                                                    title="ex: HxW: {{str_replace(","," |",$productSizesText)}}"
                                                    data-toggle="tooltip"
                                                    class="fa fa-info-circle"></span></label>
                                        <select name="size" class="select2 form-control">
                                            <option value=""></option>
                                            @foreach($productSizes as $type)
                                                <option value="{{$type['id']}}">{{$type['name']}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <label>#Nb Of Pages</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-weight-bold">#</span>
                                            </div>
                                            <input type="number" class="form-control" name="num_of_pages">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Product Files</h3>
                            <hr>

                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>E-Book/PDF Link</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text font-weight-bold"><i
                                                        class="fa fa-file-pdf"></i></span>
                                            </div>
                                            <input type="text" class="form-control" name="pdf_link">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Audio File</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text font-weight-bold"><i
                                                        class="fa fa-file-audio"></i></span>
                                            </div>
                                            <input type="text" class="form-control" name="audio">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <br>
                                    <label>Description <span class="text-danger">*</span></label>
                                    <textarea name="description" hidden id="" cols="30" rows="10"></textarea>
                                    <div id="editor">
                                        <p>
                                        </p>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-3 float-right">
                                    <button type="submit" class="btn btn-outline-info">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal crop--}}
    <div class="modal fade" id="modal" tabindex="-1" role="dialog"
         aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="max-width: 100%;">
                        <img id="image" class="image-cropper-upload" data-width="300px" data-height="300px"
                             data-crop="circle"
                             src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="button" class="btn btn-primary crop"
                            data-url="{{route('upload-image')}}">Crop
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="{{asset('js/loading-bar.js')}}"></script>


    <script>
        var bar1 = new ldBar(".ldBar");
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            init();
            $('.category-select').on('change', function () {
                let id = $(this).find('option:selected').val();
                $.ajax({
                    type: "GET",
                    url: "{{route("sub-categories.ajax")}}",
                    data: {id: id},
                    success: function (data) {
                        var option = [];
                        for (let i = 0; i < data.length; i++) {
                            option[i] = new Option(data[i].name, data[i].id, false, false);
                        }
                        $('.sub-category-select').html(option);
                    }
                })
            });
            $('.select2').select2();
        });
        init = () => {
            let toolbarOptions = [
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],

                [{'header': 1}, {'header': 2}],               // custom button values
                [{'list': 'ordered'}, {'list': 'bullet'}],
                [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
                [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
                [{'direction': 'rtl'}],                         // text direction

                [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
                [{'header': [1, 2, 3, 4, 5, 6, false]}],

                [{'color': []}, {'background': []}],          // dropdown with defaults from theme
                [{'font': []}],
                [{'align': []}],
                ['link'],

                ['clean']                                         // remove formatting button
            ];
            var text;
            let quill = new Quill('#editor', {
                modules: {
                    toolbar: toolbarOptions
                },
                theme: 'snow'
            });
            quill.on('text-change', function (e, a) {
                text = quill.root.innerHTML;
                $('textarea[name=description]').val(text)
            });
            $(".date").flatpickr({
                dateFormat: "Y-m-d",
            });
            $('.edit-image').on('click', function () {
                $('.file-input').click();
            });
        }
    </script>
    <script src="{{asset("js/cropper.js")}}"></script>
    <script src="{{asset("js/crop-image.js")}}"></script>

@endsection
@section("styles")
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/at.js/1.4.0/css/jquery.atwho.min.css">
    <link rel="stylesheet" href="{{asset("css/loading-bar.css")}}">
    <link rel="stylesheet" href="{{asset("css/cropper.css")}}">
@endsection