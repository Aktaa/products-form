<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('cover_type')->nullable();
            $table->string('isbn',50)->nullable();
            $table->string('barcode',50)->nullable();
            $table->integer('num_of_pages')->nullable();
            $table->integer('edition')->nullable();
            $table->integer('weight')->nullable();
            $table->date('release_date')->nullable();
            $table->string('size')->nullable();
            $table->string('series','25')->nullable();
            $table->string('audio','100')->nullable();
            $table->string('pdf_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_extras');
    }
}
