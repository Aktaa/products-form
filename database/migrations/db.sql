INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('1', 'Author', 'author', '2019-03-17 13:58:50', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('2', 'Co-Writer', 'author', '2019-03-17 13:58:52', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('3', 'Ghost-Writer', 'author', '2019-03-17 13:59:08', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('4', 'Translator', 'author', '2019-03-17 13:59:20', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('5', 'Illustrator', 'author', '2019-03-17 13:59:39', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('6', 'Distributor', 'publisher', '2019-03-17 14:00:25', NULL);
INSERT INTO `test`.`roles` (`id`, `name`, `model`, `created_at`, `updated_at`) VALUES ('7', 'Publisher', 'publisher', '2019-03-17 14:00:43', NULL);

INSERT INTO `test`.`authors` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES ('1', 'محمد', 'علي', '2019-03-17 14:13:15', NULL);
INSERT INTO `test`.`authors` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES ('2', ' Ahmad', 'Mohammad', '2019-03-17 14:14:02', NULL);
INSERT INTO `test`.`authors` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES ('3', 'Naser', 'Khlifi', '2019-03-17 14:14:49', NULL);
INSERT INTO `test`.`authors` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES ('4', 'Omar ', 'Khbin', '2019-03-17 14:17:39', NULL);

INSERT INTO `test`.`publishers` (`id`, `name`, `created_at`, `updated_at`) VALUES ('1', 'دار الربيع', '2019-03-17 14:12:27', NULL);
INSERT INTO `test`.`publishers` (`id`, `name`, `created_at`, `updated_at`) VALUES ('2', 'دار الفرح للمطبوعات', '2019-03-17 14:12:28', NULL);
INSERT INTO `test`.`publishers` (`id`, `name`, `created_at`, `updated_at`) VALUES ('3', 'مكتبة القوس', '2019-03-17 14:12:46', NULL);

INSERT INTO `test`.`product_types` (`id`, `name`, `created_at`, `updated_at`) VALUES ('1', 'Book', '2019-03-17 14:25:36', NULL);
INSERT INTO `test`.`product_types` (`id`, `name`, `created_at`, `updated_at`) VALUES ('2', 'Magazine', '2019-03-17 14:25:49', NULL);
INSERT INTO `test`.`product_types` (`id`, `name`, `created_at`, `updated_at`) VALUES ('3', 'Car', '2019-03-17 14:26:03', NULL);
INSERT INTO `test`.`product_types` (`id`, `name`, `created_at`, `updated_at`) VALUES ('4', 'Paper', '2019-03-17 14:26:17', NULL);

INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('1', 'أدب', NULL, '2019-03-17 14:28:19', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('2', 'فنون', NULL, '2019-03-17 14:28:20', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('3', 'أخلاق', NULL, '2019-03-17 14:28:18', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('4', 'صحف', NULL, '2019-03-17 14:28:30', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('5', 'رسم', '2', '2019-03-17 14:29:00', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('6', 'تلوين', '2', '2019-03-17 14:29:13', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('7', 'تاريخي', '1', '2019-03-17 14:29:34', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('8', 'حكايات', '1', '2019-03-17 14:29:46', NULL);
INSERT INTO `test`.`categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES ('9', 'تخطيط', '2', '2019-03-17 14:30:10', NULL);

