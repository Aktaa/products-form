<?php

namespace App\Models;


class Product extends \Eloquent
{
    protected $with = ["type", "author", "publisher", "category", "subCategory", "productExtra"];
    protected $fillable = ["name", "image", "description", "type_id", "author_id", "author_role_id", "publisher_id", "publisher_role_id", "category_id", "sub_category_id", "selling_price", "cost_price"];

    const PRODUCTS_IMAGE_PATH = "/storage/images/products/";
    const PRODUCTS_IMAGE_URL = "images/products/";



    public function type()
    {
        return self::belongsTo(ProductType::class, "type_id");
    }

    public function author()
    {
        return self::belongsTo(Author::class, "author_id");
    }

    public function publisher()
    {
        return self::belongsTo(Publisher::class, "publisher_id");
    }

    public function category()
    {
        return self::belongsTo(Category::class, "category_id");
    }

    public function subCategory()
    {
        return self::belongsTo(Category::class, "sub_category_id");
    }

    public function productExtra()
    {
        return self::hasOne(ProductExtra::class, "product_id");
    }

    public function getSellingPriceAttribute($selling_price)
    {
        return number_format($selling_price);
    }

    public function getCostPriceAttribute($cost_price)
    {
        return number_format($cost_price);
    }
}
