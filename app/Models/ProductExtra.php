<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExtra extends Model
{
    protected $fillable = ["cover_type", "product_id", "isbn", "barcode", "num_of_pages", "weight", "release_date", "size", "series", "audio", "pdf_link", "edition"];

    public function getWeightAttribute($weight)
    {
        return !is_null($weight) ? number_format($weight) . " gm" : "-";
    }

    public function getSeriesAttribute($series)
    {
        return !is_null($series) ? $series : "-";
    }

    public function getNumOfPagesAttribute($num_of_pages)
    {
        return !is_null($num_of_pages) ? $num_of_pages : "-";
    }

    public function getReleaseDateAttribute($release_date)
    {
        return !is_null($release_date) ? $release_date : "-";
    }

    public function getPdfLinkAttribute($pdf_link)
    {
        return !is_null($pdf_link) ? $pdf_link : "-";
    }

    public function getEditionAttribute($edition)
    {
        return !is_null($edition) ? $edition : "-";
    }

}
