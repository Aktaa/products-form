<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 * @package App\Models
 * @property string $first_name
 * @property string $last_name
 * @property int $role_id
 */
class Author extends Model
{
    protected $fillable = ["first_name", "last_name"];

    protected $appends = ["full_name"];

//    public function role()
//    {
//        return self::belongsTo(Role::class, "role_id")->where("model", "author");
//    }

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

}
