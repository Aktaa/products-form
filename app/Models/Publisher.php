<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = ["name"];

//    public function role()
//    {
//        return self::belongsTo(Role::class, "role_id")->where("model", "publisher");
//    }
}
