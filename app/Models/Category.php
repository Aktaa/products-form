<?php

namespace App\Models;


class Category extends \Eloquent
{
    protected $fillable = ['name', 'parent_id'];

    public function product()
    {
        return self::hasMany(Product::class, 'category_id', 'id');
    }

    public function parent()
    {
        return self::belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return self::hasMany(self::class, 'parent_id', 'id');
    }

}
