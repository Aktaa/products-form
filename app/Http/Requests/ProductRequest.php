<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:254",
            "type_id" => "required",
            "description" => "required",
            "author_id" => "required",
            "author_role_id" => "required",
            "publisher_id" => "required",
            "publisher_role_id" => "required",
            "category_id" => "required",
            "selling_price" => "required|numeric",
            "cost_price" => "required|numeric"
        ];
    }
}
