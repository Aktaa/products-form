<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Author;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Publisher;
use App\Models\Role;
use App\Repositories\Classes\ProductsRepository;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class ProductController extends Controller
{
    public $repo;

    public function __construct(ProductsRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->repo->all();
        return view("pages.products", compact("products"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles_author_text = $roles_publisher_text = $product_types_text = "";
        //data
        $authors = Author::all();
        $publishers = Publisher::all();
        $author_roles = Role::where("model", "author")->get();
        $publishers_roles = Role::where("model", "publisher")->get();
        $product_types = ProductType::all();
        $categories = Category::whereNull('parent_id')->get();

        //text for tooltips
        $roles_author_text = getTextFromCollect($author_roles);
        $roles_publisher_text = getTextFromCollect($publishers_roles);
        $product_types_text = getTextFromCollect($product_types);
        return view("pages.add", compact("authors", "categories", "author_roles", "roles_author_text", "publishers", "publishers_roles", "roles_publisher_text", "product_types", "product_types_text"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(ProductRequest $request)
    {
        $valid = $request->validated();
        $product = $this->repo->create($request->all());
        $this->repo->createProductExtra($product, $request->all());
        if ($request->filled('image'))
            $this->repo->moveFileFromTemp(Product::PRODUCTS_IMAGE_URL, $request->get('image'));
        $products = $this->repo->all();
        return redirect(route("products"))->with("products", $products);
    }


    public function savePhotoInTemp(Request $request)
    {
        $image_name = "";
        if ($request->has('image')) {
            $manager = new ImageManager(array('driver' => 'gd'));
            $image = $manager->make($request->get('image', ''));
            $fileTempName = sha1(time() . (int)rand(10000, 1000000)) . '.' . explode('/', $image->mime())[1];
            $image->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/public/temp/' . $fileTempName));
            $image_name = $fileTempName;
        }
        return ["name" => $image_name];
    }
}
