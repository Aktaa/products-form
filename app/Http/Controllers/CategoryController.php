<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function get(Request $request)
    {
         $id=(int)$request->get('id');
         $data=Category::where("parent_id",$id)->get();
         return $data;
    }
}
