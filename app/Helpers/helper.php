<?php
/**
 * Created by PhpStorm.
 * User: Mohammad
 * Date: 3/17/2019
 * Time: 6:01 PM
 */
if (!function_exists("getTextFromCollect")) {

    /**
     * @param $collect
     * @return string
     */
    function getTextFromCollect($collect): string
    {
        $roles = "";
        foreach ($collect as $key => $item) {
            if ($key == $collect->count() - 1)
                $roles .= $item->name;
            else
                $roles .= $item->name . ", ";
        }
        return $roles;
    }
}
if (!function_exists("getTextFromArray")) {

    /**
     * @param $array
     * @return string
     */
    function getTextFromArray($array): string
    {
        $roles = "";
        foreach ($array as $key => $item) {
            if ($key == $array->count() - 1)
                $roles .= $item["name"];
            else
                $roles .= $item["name"] . ", ";
        }
        return $roles;
    }
}