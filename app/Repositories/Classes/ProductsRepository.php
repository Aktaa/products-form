<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 7/5/2018
 * Time: 4:48 AM
 */

namespace App\Repositories\Classes;


use App\Models\Product;
use App\Repositories\interfaces\RepositoryInterface;

class ProductsRepository implements RepositoryInterface
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
        return $this->product->all();
        // TODO: Implement all() method.
    }

    public function create($data)
    {
        return Product::create($data);
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    function moveFileFromTemp($newPath, $name, $newName = '')
    {
        if ($newName == '') $newName = $name;
        \File::move(storage_path('app/public/temp/' . $name), storage_path('app/public/' . $newPath) . $newName);
    }

    public function productExtra()
    {
        return $this->product->productExtra();
    }

    public function createProductExtra($product, $data)
    {
        return $product->productExtra()->create($data);
    }
}