<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 7/5/2018
 * Time: 4:44 AM
 */

namespace App\Repositories\interfaces;


interface RepositoryInterface
{
    public function all();

    public function create($data);

    public function update($data, $id);

    public function delete($id);

    public function show($id);
}