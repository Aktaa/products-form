<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        $coverTypes=collect([["id"=>0,"name"=>"Normal"],["id"=>1,"name"=>"Art"]]);
        $productSizes=collect([["id"=>0,"name"=>"17*12"],["id"=>1,"name"=>"17*15"],["id"=>2,"name"=>"20*14"],["id"=>3,"name"=>"20*27"]]);
        \View::share("coverTypes", $coverTypes);
        \View::share("coverTypesText",getTextFromArray($coverTypes));
        \View::share("productSizes", $productSizes);
        \View::share("productSizesText",getTextFromArray($productSizes));
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
